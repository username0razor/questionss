package com.project.question.model;

import com.project.question.entity.Student;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * Created by shivam on 26/08/18
 */
@Getter
@Setter
@NoArgsConstructor
public class StudentDTO {

  private Long id;

  private String name;

  private Integer grade;

  private Boolean active;

  private Date admissionYear;

  public StudentDTO(Student student){
    this.id = student.getId();
    this.name = student.getName();
    this.grade = student.getGrade();
    this.active = student.getActive() != null ? student.getActive() : Boolean.TRUE;
    this.admissionYear = student.getAdmissionYear();
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
