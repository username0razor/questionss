package com.project.question.service.impl;

import com.project.question.constants.SearchContextModel;
import com.project.question.entity.Student;
import com.project.question.model.StudentDTO;
import com.project.question.repository.StudentRepository;
import com.project.question.response.CommonResponseObject;
import com.project.question.service.StudentService;
import com.project.question.utils.SpecificationUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by shivam on 26/08/18
 */
@Service("com.project.question.service.impl.StudentServiceImpl")
@Slf4j
public class StudentServiceImpl implements StudentService {


  @Autowired
  @Qualifier("com.project.question.repository.StudentRepository")
  private StudentRepository studentRepository;

  @Override
  public CommonResponseObject<StudentDTO> createStudent(StudentDTO studentDTO) {
    CommonResponseObject<StudentDTO> responseObject =
        new CommonResponseObject<>(HttpStatus.CREATED);
    try {
      Student savedStudent = studentRepository.save(new Student(studentDTO));
      responseObject.setResponseObject(new StudentDTO(savedStudent));
    } catch (Exception ex) {
      log.error("Failed to create Student due to exception ", ex.getMessage());
      responseObject.setStatusCode(HttpStatus.OK);
    }
    return responseObject;
  }

  @Override
  public CommonResponseObject<StudentDTO> deleteStudent(Long id) {
    CommonResponseObject<StudentDTO> responseObject =
        new CommonResponseObject<>(HttpStatus.NOT_FOUND);
    Student studentToDelete = studentRepository.findOne(id);
    if (studentToDelete != null) {
      studentToDelete.setActive(Boolean.FALSE);
      studentRepository.save(studentToDelete);
      log.info("Updated Student :{}, marked status as Inactive", id);
      responseObject.setResponseObject(new StudentDTO(studentToDelete));
      responseObject.setStatusCode(HttpStatus.OK);
    }
    return responseObject;
  }

  @Override
  public CommonResponseObject<StudentDTO> viewStudent(Long id) {
    CommonResponseObject<StudentDTO> responseObject =
        new CommonResponseObject<>(HttpStatus.NOT_FOUND);
    Student student = studentRepository.findOne(id);
    if (student != null) {
      responseObject.setResponseObject(new StudentDTO(student));
      responseObject.setStatusCode(HttpStatus.OK);
    }
    return responseObject;
  }

  /**
   * Update student's grade
   * @param id
   * @param studentDTO
   * @return
   */
  @Override
  public CommonResponseObject<StudentDTO> updateStudentGrade(Long id, StudentDTO studentDTO) {
    CommonResponseObject<StudentDTO> responseObject =
        new CommonResponseObject<>(HttpStatus.NOT_FOUND);
    Student student = studentRepository.findOne(id);
    if (student != null) {
      student.setGrade(studentDTO.getGrade());
      studentRepository.save(student);
      log.info("Update Student :{} Grade from previous value :{} to new value :{}", student.getId(),
          student.getGrade(), studentDTO.getGrade());
      responseObject.setStatusCode(HttpStatus.OK);
      responseObject.setResponseObject(new StudentDTO(student));
    }
    return responseObject;
  }

  /**
   * GET Students from the system based on the filter criteria
   * @param pageable
   * @param searchContextModel
   * @return
   */
  @Override
  public CommonResponseObject<Page<StudentDTO>> getAllStudents(Pageable pageable, SearchContextModel searchContextModel) {
    Specification specification = SpecificationUtility.getSpecification(searchContextModel);
    Page<Student> students = studentRepository.findAll(specification, pageable);
    CommonResponseObject<Page<StudentDTO>> responseObject = new CommonResponseObject<>(HttpStatus.OK);
    Page<StudentDTO> studentDTOS = new PageImpl<>(Collections.emptyList());
    responseObject.setResponseObject(studentDTOS);
    if(!CollectionUtils.isEmpty(students.getContent())){
      List<StudentDTO> studentDTOList = new ArrayList<>();
      students.getContent().stream().forEach(student -> {
        studentDTOList.add(new StudentDTO(student));
      });
      responseObject.setResponseObject(new PageImpl<>(studentDTOList));
    }
    return responseObject;
  }
}
