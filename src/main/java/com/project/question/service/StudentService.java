package com.project.question.service;

import com.project.question.constants.SearchContextModel;
import com.project.question.model.StudentDTO;
import com.project.question.response.CommonResponseObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by shivam on 26/08/18
 */
public interface StudentService {
  CommonResponseObject<StudentDTO> createStudent(StudentDTO studentDTO);
  CommonResponseObject<StudentDTO> deleteStudent(Long id);
  CommonResponseObject<StudentDTO> viewStudent(Long id);
  CommonResponseObject<StudentDTO> updateStudentGrade(Long id, StudentDTO studentDTO);
  CommonResponseObject<Page<StudentDTO>> getAllStudents(Pageable pageable, SearchContextModel searchContextModel);
}
