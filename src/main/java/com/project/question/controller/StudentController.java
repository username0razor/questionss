package com.project.question.controller;

import com.project.question.constants.SearchContextModel;
import com.project.question.model.StudentDTO;
import com.project.question.response.CommonResponseObject;
import com.project.question.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.List;

/**
 * Created by shivam on 26/08/18
 */
@RestController
@RequestMapping(value = "/v1")
@Slf4j
public class StudentController {

  @Autowired
  @Qualifier("com.project.question.service.impl.StudentServiceImpl")
  private StudentService studentService;

  @RequestMapping(value = "/students", method = RequestMethod.POST)
  public ResponseEntity<CommonResponseObject> createStudent(@RequestBody StudentDTO studentDTO){
    log.info("Requested to create Student with name:{}", studentDTO.getName());
    CommonResponseObject<StudentDTO> student = studentService.createStudent(studentDTO);
    return new ResponseEntity<>(student, student.getStatusCode());
  }

  @RequestMapping(value = "/students/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<CommonResponseObject> deleteStudent(@PathVariable("id") Long id){
    log.info("Requested to delete Student with id:{}", id);
    CommonResponseObject<StudentDTO> student = studentService.deleteStudent(id);
    return new ResponseEntity<>(student, student.getStatusCode());
  }

  @RequestMapping(value = "/students/{id}", method = RequestMethod.GET)
  public ResponseEntity<CommonResponseObject> getStudent(@PathVariable("id") Long id){
    log.info("Requested to Get Student with id:{}", id);
    CommonResponseObject<StudentDTO> student = studentService.viewStudent(id);
    return new ResponseEntity<>(student, student.getStatusCode());
  }

  @RequestMapping(value = "/students/{id}", method = RequestMethod.PATCH)
  public ResponseEntity<CommonResponseObject> patchStudent(@PathVariable("id") Long id,
      @RequestBody StudentDTO studentDTO){
    log.info("Requested to Update Student's Grade/class with id:{}", id);
    CommonResponseObject<StudentDTO> student = studentService.updateStudentGrade(id, studentDTO);
    return new ResponseEntity<>(student, student.getStatusCode());
  }

  @RequestMapping(value = "/students", method = RequestMethod.GET)
  public ResponseEntity<CommonResponseObject> getAllStudent(
      @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
      @RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
      @RequestParam(value = "classes", required = false) List<Integer> classes,
      @RequestParam(value = "active", required = false) Boolean active,
      @RequestParam(value = "admissionYearBefore", required = false) String admissionYearBefore,
      @RequestParam(value = "admissionYearAfter", required = false) String admissionYearAfter
      ) {
    log.info("Requested to get all students");
    /**
     * pageNumber - 1 ; because page index starts from 0;
     * TODO: code will break for less than 0
     */
    Pageable pageable = new PageRequest(pageNumber - 1 , pageSize < 1 ? 1 : pageSize);
    CommonResponseObject<Page<StudentDTO>> studentList = new CommonResponseObject<>(HttpStatus.OK);
    try {
      studentList = studentService.getAllStudents(pageable,
          new SearchContextModel(classes, active, admissionYearBefore, admissionYearAfter));
    } catch (ParseException e) {
      log.error("Year Format not accepted");
    }
    return new ResponseEntity<>(studentList, studentList.getStatusCode());
  }
}
