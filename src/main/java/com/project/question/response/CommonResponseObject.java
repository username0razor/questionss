package com.project.question.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

/**
 * Created by shivam on 26/08/18
 */
@Getter
@Setter
@ToString(doNotUseGetters = true)
public class CommonResponseObject<T> {

  private T responseObject;
  private HttpStatus statusCode;

  public CommonResponseObject(HttpStatus status) {
    this.statusCode = status;
  }
}
