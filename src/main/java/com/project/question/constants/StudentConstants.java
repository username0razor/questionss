package com.project.question.constants;

/**
 * Created by shivam on 26/08/18
 */
public class StudentConstants {
  public static final String STUDENT_TABLE = "STUDENT";
  public static final String NAME = "name";
  public static final String GRADE = "grade";
  public static final String ACTIVE = "active";
  public static final String ADMISSION_YEAR = "admissionYear";
}
