package com.project.question.constants;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by shivam on 26/08/18
 */
@Getter
@Setter
@NoArgsConstructor
public class SearchContextModel {

  private static final String ADMISSION_YEAR_BEFORE_DATE_PREFIX = "31-12-";
  private static final String ADMISSION_YEAR_AFTER_DATE_PREFIX = "01-01-";
  private static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
  private static final String YEAR_FORMAT = "Incorrect Year format";

  private List<Integer> classes;
  private Boolean active;
  private Date admissionYearBefore;
  private Date admissionYearAfter;

  public SearchContextModel(List<Integer> classes, Boolean active, String admissionYearBefore,
      String admissionYearAfter) throws ParseException {
    this.classes = classes;
    this.active = active;
    if (StringUtils.isNotEmpty(admissionYearAfter)) {
      if (admissionYearAfter.length() == 4)
        this.admissionYearAfter =
            dateFormat.parse(ADMISSION_YEAR_AFTER_DATE_PREFIX + admissionYearAfter);
      else
        throw new IllegalArgumentException(YEAR_FORMAT);

    }
    if (StringUtils.isNotEmpty(admissionYearBefore)) {
      if (admissionYearBefore.length() == 4)
        this.admissionYearBefore =
            dateFormat.parse(ADMISSION_YEAR_BEFORE_DATE_PREFIX + admissionYearBefore);
      else
        throw new IllegalArgumentException(YEAR_FORMAT);

    }
  }
}
