package com.project.question.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by shivam on 27/08/18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchCriteria {
  /**
   * Search Criteria class
   * key : fieldName
   * operation : greaterThan(>), Equals(:), lessThan(<)
   * value : parameter value from the request
   */
  private String key;
  private String operation;
  private Object value;
}
