package com.project.question.repository;

import com.project.question.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created by shivam on 26/08/18
 */
@Repository("com.project.question.repository.StudentRepository")
public interface StudentRepository extends JpaRepository<Student, Long>,
    JpaSpecificationExecutor<Student> {
}
