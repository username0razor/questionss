package com.project.question.entity;

import com.project.question.constants.StudentConstants;
import com.project.question.model.StudentDTO;
import com.project.question.utils.ValidationUtility;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by shivam on 26/08/18
 */
@Entity
@Table(name = StudentConstants.STUDENT_TABLE)
@Getter
@Setter
@NoArgsConstructor
public class Student {

  @Id
  @GeneratedValue
  private Long id;

  @Column(name = StudentConstants.NAME)
  private String name;

  @Column(name = StudentConstants.GRADE)
  private Integer grade;

  @Column(name = StudentConstants.ACTIVE)
  private Boolean active;

  @Column(name = StudentConstants.ADMISSION_YEAR)
  private Date admissionYear;

  public Student(StudentDTO studentDTO) {
    if (ValidationUtility.validateFields(studentDTO)) {
      this.id = studentDTO.getId();
      this.name = studentDTO.getName();
      this.grade = studentDTO.getGrade();
      this.active = studentDTO.getActive() == null ? Boolean.TRUE : studentDTO.getActive();
      this.admissionYear = studentDTO.getAdmissionYear();
    }
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
