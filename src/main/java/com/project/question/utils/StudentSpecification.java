package com.project.question.utils;

import com.project.question.constants.SearchCriteria;
import com.project.question.entity.Student;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shivam on 27/08/18
 */
public class StudentSpecification implements Specification<Student> {

  private SearchCriteria searchCriteria;

  public StudentSpecification(SearchCriteria searchCriteria) {
    this.searchCriteria = searchCriteria;
  }

  /**
   * Creates a WHERE clause for a query of the referenced entity in form of a {@link Predicate} for the given
   * {@link Root} and {@link CriteriaQuery}.
   *
   * @param root
   * @param query
   * @param builder
   * @return a {@link Predicate}, may be {@literal null}.
   */
  @Override
  public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query,
      CriteriaBuilder builder) {

    if (searchCriteria.getOperation().equalsIgnoreCase(">")) {
      if (searchCriteria.getKey().equalsIgnoreCase("admissionYear")) {
        return builder.greaterThanOrEqualTo(root.<Date>get("admissionYear"),
            (Date) searchCriteria.getValue());
      }
    } else if (searchCriteria.getOperation().equalsIgnoreCase("<")) {
      if (searchCriteria.getKey().equalsIgnoreCase("admissionYear")) {
        return builder
            .lessThanOrEqualTo(root.<Date>get("admissionYear"), (Date) searchCriteria.getValue());
      }
    } else if (searchCriteria.getOperation().equalsIgnoreCase(":")) {
      if (root.get(searchCriteria.getKey()).getJavaType() == String.class) {
        return builder
            .like(root.<String>get(searchCriteria.getKey()), "%" + searchCriteria.getValue() + "%");
      } else {
        return builder.equal(root.get(searchCriteria.getKey()), searchCriteria.getValue());
      }
    } else if (searchCriteria.getOperation().equalsIgnoreCase("in")) {
      final List<Predicate> orPredicates = new ArrayList<Predicate>();
      List<Integer> viewIds = (List<Integer>) searchCriteria.getValue();
      for (Integer classId : viewIds) {
        orPredicates
            .add(builder.or(builder.equal(root.<String>get(searchCriteria.getKey()), classId)));
      }
      return builder.or(orPredicates.toArray(new Predicate[orPredicates.size()]));
    }
    return null;
  }
}
