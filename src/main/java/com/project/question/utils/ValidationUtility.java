package com.project.question.utils;

import com.project.question.model.StudentDTO;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by shivam on 26/08/18
 */
public class ValidationUtility {

  public static Boolean validateFields(StudentDTO studentDTO) {
    if (StringUtils.isNotEmpty(studentDTO.getName()) && studentDTO.getGrade() != null
        && studentDTO.getGrade() > 0 && studentDTO.getAdmissionYear() != null) {
      return true;
    } else {
      throw new IllegalArgumentException("Invalid/Incomplete Student Request Detail");
    }
  }
}
