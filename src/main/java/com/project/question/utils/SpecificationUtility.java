package com.project.question.utils;

import com.project.question.constants.SearchContextModel;
import com.project.question.constants.SearchCriteria;
import com.project.question.entity.Student;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.util.CollectionUtils;

/**
 * Created by shivam on 27/08/18
 */
public class SpecificationUtility {

  /**
   * Construct specification based on the search context parameters
   * @param searchContextModel
   * @return
   */
  public static Specifications<Student> getSpecification(SearchContextModel searchContextModel) {

    Specifications<Student> studentSpecifications = null;

    //Adding In Clause criteria for Grade filter
    if (!CollectionUtils.isEmpty(searchContextModel.getClasses())) {
      StudentSpecification gradeSpec = new StudentSpecification(
          new SearchCriteria("grade", "in", searchContextModel.getClasses()));
      studentSpecifications = Specifications.where(gradeSpec);
    }

    //Adding Active status criteria for T/F
    if (searchContextModel.getActive() != null) {
      StudentSpecification activeSpec = new StudentSpecification(
          new SearchCriteria("active", ":", searchContextModel.getActive()));
      if (studentSpecifications != null) {
        studentSpecifications = studentSpecifications.and(activeSpec);
      }else {
        studentSpecifications = Specifications.where(activeSpec);
      }
    }

    //Adding admissionYearBefore admission Year criteria
    if (searchContextModel.getAdmissionYearBefore() != null) {
      StudentSpecification dateBeforeSpec = new StudentSpecification(
          new SearchCriteria("admissionYear", "<", searchContextModel.getAdmissionYearBefore()));
      if (studentSpecifications != null) {
        studentSpecifications = studentSpecifications.and(dateBeforeSpec);
      }else {
        studentSpecifications = Specifications.where(dateBeforeSpec);
      }
    }

    //Adding admissionYearAfter admission Year criteria
    if (searchContextModel.getAdmissionYearAfter() != null) {
      StudentSpecification dateAfterSpec = new StudentSpecification(
          new SearchCriteria("admissionYear", ">", searchContextModel.getAdmissionYearAfter()));
      if (studentSpecifications != null) {
        studentSpecifications = studentSpecifications.and(dateAfterSpec);
      }else {
        studentSpecifications = Specifications.where(dateAfterSpec);
      }
    }

    return studentSpecifications;
  }
}
