FROM anapsix/alpine-java:latest
ARG RUNENV=application
ARG SNAPSHOT=0.0.1-SNAPSHOT
ENV RUNENV=${RUNENV}
ENV APPNAME=question
ENV SNAPSHOT=${SNAPSHOT}
RUN mkdir /app
RUN mkdir app/config
COPY build/libs/question-${SNAPSHOT}.jar /app
COPY config /app/config
EXPOSE 8111
WORKDIR /app
ENTRYPOINT exec java -jar -server -Xmx768m -Xss256k -Xms256m -XX:MetaspaceSize=64m -XX:MaxMetaspaceSize=150m -XX:CompressedClassSpaceSize=32m -XX:ReservedCodeCacheSize=16m -XX:+UseG1GC -XX:MaxGCPauseMillis=20 -XX:+UseStringDeduplication -Dspring.profiles.active=${RUNENV} ${APPNAME}-${SNAPSHOT}.jar